package testing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import modelGUI.Gui3;


public class Test3 {

	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {

			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);

		}

	}

	public static void main(String[] args) {

		new Test3();
	}

	public Test3() {
		frame = new Gui3();
	}
	
	ActionListener list;
	Gui3 frame;
}
