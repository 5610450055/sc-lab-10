package testing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import modelGUI.Gui2;


public class Test2 {

	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {

			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);

		}

	}

	public static void main(String[] args) {

		new Test2();
	}

	public Test2() {
		frame = new Gui2();
	}
	
	ActionListener list;
	Gui2 frame;
}
