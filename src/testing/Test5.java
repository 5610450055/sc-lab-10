package testing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import modelGUI.Gui5;


public class Test5 {

	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {

			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);

		}

	}

	public static void main(String[] args) {

		new Test5();
	}

	public Test5() {
		frame = new Gui5();
	}
	
	ActionListener list;
	Gui5 frame;
}
