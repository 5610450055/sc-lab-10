package testing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import modelGUI.Gui4;


public class Test4 {

	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {

			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);

		}

	}

	public static void main(String[] args) {

		new Test4();
	}

	public Test4() {
		frame = new Gui4();
	}
	
	ActionListener list;
	Gui4 frame;
}
