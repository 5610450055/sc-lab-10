package testing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import modelGUI.Gui1;


public class Test1 {

	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {

			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);

		}

	}

	public static void main(String[] args) {

		new Test1();
	}

	public Test1() {
		frame = new Gui1();
	}
	
	ActionListener list;
	Gui1 frame;
}
