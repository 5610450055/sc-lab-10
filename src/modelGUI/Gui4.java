package modelGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JComboBox;

public class Gui4 extends JFrame {
	
	private JComboBox button;
	private JFrame frame;
	private JPanel background;
	private JPanel panel1;
	
	public Gui4() {
		createFrame();
	}

	public void createFrame() {
		frame = new JFrame();
		createPanel();
		frame.add(background);
		frame.setSize(600, 600);
		frame.setVisible(true);
		
	}
	
	public void createButton(){
		button = new JComboBox<>();
		button.addItem("RED");
		button.addItem("GREEN");
		button.addItem("BLUE");
		button.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			if (button.getSelectedItem()=="RED") {
				
				background.setBackground(Color.RED);
			}
			else if (button.getSelectedItem()=="GREEN") {
				
				background.setBackground(Color.GREEN);
			}
			else if (button.getSelectedItem()=="BLUE") {
				
				background.setBackground(Color.BLUE);
				}
			}
		});
	}
	
	public void createPanel(){
		panel1 = new JPanel();
		background = new JPanel();
		background.setLayout(new BorderLayout());
		createButton();
		panel1.add(button);
		background.add(panel1, BorderLayout.SOUTH);

	}
	
	
}
