package modelGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;

public class Gui2 extends JFrame {
	
	private JRadioButton rButton;
	private JRadioButton gButton;
	private JRadioButton bButton;
	private JFrame frame;
	private JPanel background;
	private JPanel panel1;
	private JPanel panel2;
	
	public Gui2() {
		createFrame();
	}

	public void createFrame() {
		frame = new JFrame();
		createPanel();
		frame.add(background);
		frame.setSize(600, 600);
		frame.setVisible(true);
		
	}
	
	public void createButton(){
		rButton = new JRadioButton("RED");
		gButton = new JRadioButton("GREEN");
		bButton = new JRadioButton("BLUE");
		rButton.addActionListener(new ActionListener(){
			
			@Override
			public void actionPerformed(ActionEvent e) {
				background.setBackground(Color.RED);
			}
			
		});
		
		gButton.addActionListener(new ActionListener(){
			
			@Override
			public void actionPerformed(ActionEvent e) {
				background.setBackground(Color.GREEN);
			}
			
		});
		
		bButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				background.setBackground(Color.BLUE);
			}
			
		});
	}
	
	public void createPanel(){
		panel1 = new JPanel();
		panel2 = new JPanel();
		background = new JPanel();
		background.setLayout(new BorderLayout());
		ButtonGroup group = new ButtonGroup();
		createButton();
		group.add(rButton);
		group.add(gButton);
		group.add(bButton);
		panel1.add(rButton);
		panel1.add(gButton);
		panel1.add(bButton);
		background.add(panel1, BorderLayout.SOUTH);
		//background.add(panel2, BorderLayout.WEST);

	}
	
	
}
