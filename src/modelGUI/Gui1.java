package modelGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class Gui1 extends JFrame {
	
	private JButton button1;
	private JButton button2;
	private JButton button3;
	private JFrame frame;
	private JPanel background;
	private JPanel panel1;
	private JPanel panel2;
	
	public Gui1() {
		createFrame();
	}

	public void createFrame() {
		frame = new JFrame();
		createPanel();
		frame.add(background);
		frame.setSize(600, 600);
		frame.setVisible(true);
		
	}
	
	public void createPanel(){
		panel1 = new JPanel();
		panel2 = new JPanel();
		background = new JPanel();
		background.setLayout(new BorderLayout());
		createButton();
		panel1.add(button1);
		panel1.add(button2);
		panel1.add(button3);
		background.add(panel1, BorderLayout.SOUTH);
		//background.add(panel2, BorderLayout.WEST);

	}
	
	public void createButton(){
		button1 = new JButton("RED");
		button2 = new JButton("GREEN");
		button3 = new JButton("BLUE");
		button1.addActionListener(new ActionListener(){
			
			@Override
			public void actionPerformed(ActionEvent e) {
				background.setBackground(Color.RED);
			}
			
		});
		
		button2.addActionListener(new ActionListener(){
			
			@Override
			public void actionPerformed(ActionEvent e) {
				background.setBackground(Color.GREEN);
			}
			
		});
		
		button3.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				background.setBackground(Color.BLUE);
			}
			
		});
}
}
