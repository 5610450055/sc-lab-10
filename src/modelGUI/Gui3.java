package modelGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JCheckBox;

public class Gui3 extends JFrame {
	
	private JCheckBox rButton;
	private JCheckBox gButton;
	private JCheckBox bButton;
	private JFrame frame;
	private JPanel background;
	private JPanel panel1;
	private JPanel panel2;
	
	public Gui3() {
		createFrame();
	}

	public void createFrame() {
		frame = new JFrame();
		createPanel();
		frame.add(background);
		frame.setSize(600, 600);
		frame.setVisible(true);
		
	}
	
	public void createButton(){
		rButton = new JCheckBox("RED");
		gButton = new JCheckBox("GREEN");
		bButton = new JCheckBox("BLUE");
		
		rButton.addActionListener(new ActionListener(){
			
			@Override
			public void actionPerformed(ActionEvent e) {
				background.setBackground(Color.RED);
			}
			
		});
		
		gButton.addActionListener(new ActionListener(){
			
			@Override
			public void actionPerformed(ActionEvent e) {
				background.setBackground(Color.GREEN);
			}
			
		});
		
		bButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				background.setBackground(Color.BLUE);
			}
			
		});
		
	}
	
	public void createPanel(){
		panel1 = new JPanel();
		panel2 = new JPanel();
		background = new JPanel();
		background.setLayout(new BorderLayout());
		createButton();
		panel1.add(rButton);
		panel1.add(gButton);
		panel1.add(bButton);
		background.add(panel1, BorderLayout.SOUTH);
		//background.add(panel2, BorderLayout.WEST);

	}
	
	
}
