package modelGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class Gui5 extends JFrame {
	
	private JMenu menu;
	private JMenuBar menuBar;
	private JMenuItem menuRED;
	private JMenuItem menuGREEN;
	private JMenuItem menuBLUE;
	private JFrame frame;
	private JPanel background;
	
	public Gui5() {
		createFrame();
	}

	public void createFrame() {
		frame = new JFrame();
		createPanel();
		frame.add(background);
		frame.setSize(600, 600);
		frame.setVisible(true);
		
	}
	
	public void createButton(){
		menu = new JMenu("Menu");
		menuBar = new JMenuBar();
		menuRED = new JMenuItem("RED");
		menuGREEN = new JMenuItem("GREEN");
		menuBLUE = new JMenuItem("BLUE");
		menu.add(menuRED);
		menu.add(menuGREEN);
		menu.add(menuBLUE);
		menuBar.add(menu);
		
		menuRED.addActionListener(new ActionListener(){
			
			@Override
			public void actionPerformed(ActionEvent e) {
				background.setBackground(Color.RED);
			}
			
		});
		
		menuGREEN.addActionListener(new ActionListener(){
			
			@Override
			public void actionPerformed(ActionEvent e) {
				background.setBackground(Color.GREEN);
			}
			
		});
		
		menuBLUE.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				background.setBackground(Color.BLUE);
			}
			
		});
	}
	
	public void createPanel(){
		background = new JPanel();
		background.setLayout(new BorderLayout());
		createButton();
		background.add(menuBar, BorderLayout.SOUTH);
	}
	
	
}
